export interface IProjectConfig {
    src: string;
    alt: string;
    projectLink?: string;
    title: string;
    description: string;
    tech: string[];
}
