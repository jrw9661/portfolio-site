import App from 'src/App.svelte';
import { CLIMBING_SPOT_LINK, DRUM_MACHINE_LINK } from './constants';
import type { IProjectConfig } from './projectsConfig.d';

const PROJECTS: IProjectConfig[] = [
    {
      src: "images/BeatMaker.png",
      alt: "Beat Maker screenshot",
      projectLink: DRUM_MACHINE_LINK,
      title: "Beat Maker 5000",
      description: `Beat Maker 5000 was a fun experiment to combine my love for music and
      problem solving skills to create a sample based drum machine. Users
      can create their own beats by recording live play or clicking
      individual notes per each sound. Beat Maker supports holding up to 16
      patterns and users can drag their own sound samples in.`,
      tech: ["Reactjs", "Recoil", "Tone.js", "Firebase", "Tailwind"],
    },
    {
      src: "images/BudgetBuddy.jpg",
      alt: "Budget Buddy screenshot",
      title:"Budget Buddy",
      description: `Whether it was necessity or frugality, Budget Buddy was my personal take on creating a budgeting site. Based on the popular zero-based budgeting plan, the goal was to make it easy to track monthly expenses and income and ensure that every dollar has a job. The site includes graphs to see current and projected spending, saving goals, current savings based on categories and budget designer.`,
      tech: ["Reactjs", "Redux", "Firebase", "Material UI"]
    },
    {
      src: "images/ClimbingSpot.jpg",
      alt: "The Climbing Spot screenshot",
      projectLink: CLIMBING_SPOT_LINK,
      title: "The Climbing Spot",
      description: `Rock climbing has been a big passion of mine. I love the physicality of solving puzzles with your body and the welcoming, encouraging community that makes it exciting to come back to every day. The Climbing Spot allows users to search for their local spot, find the climbs that they want, and put in a time for when they expect to be there. The Climbing Spot has powerful search features making it easy to find the exact climb, difficulty, and rating that they’re looking for.`,
      tech: ["Reactjs", "Firebase", "Material UI"]
    }
  ];

const app = new App({
	target: document.body,
	props: {
		projects: PROJECTS,
	},
  intro: true

});

export default app;