export const LINKEDIN_LINK = 'https://www.linkedin.com/in/jonathan-wong96/';

export const GITHUB_LINK = 'https://github.com/wongJonathan';

export const DRUM_MACHINE_LINK = 'https://drum-machine-90051.web.app/';

export const CLIMBING_SPOT_LINK = 'https://theclimbingspot.com/';

export const EMAIL_LINK = 'wong.jonathan@live.com'

export const SKILLS = [
  "JavaScript",
  "TypeScript",
  "React",
  "Redux",
  "Recoil",
  "Svelte",
  "Firebase",
  "Java",
  "Python",
  "Html",
  "Css",
  "Sass/Scss",
  "AG Grid",
  "AMPS",
  "Highcharts",
  "Tailwind",
];